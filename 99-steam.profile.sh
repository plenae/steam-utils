# Put this file in /etc/profile.d
# This provides an alias to chown the steam library

alias steam_owner='sudo chown -R $USER:users /home/shared/steam'
