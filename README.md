# Steam utils

This repository contains some very small tools to use with steam on a linux system.

## Library ownership
A steam library can be shared across user accounts on a linux system, but ownership is an issue. Group permissions don't suffice, the user running steam MUST be the owner of the steam library.

To make that easy you can combine a sudo rule to change the ownership of the library with an alias definition in the shared profile.d which most users should load at the start of their shell.

It's probably imprudent to change ownership at X session startup, since multiple users can use the system concurrently. It may be interesting to use a small shell script which checks if no other users are using the steam library prior to changing ownership.
